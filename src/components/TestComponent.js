import React, { Component } from 'react'
import AppStore from "../stores/AppStore";

class TestComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account_id: AppStore.getAccount_id()
        }
    }

    componentDidMount() {
        AppStore.on("storeUpdated", this.updateAccount_id);
    }

    componentWillUnmount() {
        AppStore.removeListener("storeUpdated", this.updateAccount_id);
    }

    updateAccount_id = () => {
        this.setState({account_id: AppStore.getAccount_id()})
    };

    render() {
        return (
            <div>Account ID: {this.state.account_id} </div>
        )
    }
}

export default TestComponent
