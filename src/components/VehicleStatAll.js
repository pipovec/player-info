import React, { Component } from 'react';
import Winrate from '../library/Winrate.js'
import TankName from '../library/TankName.js'
import TankLevel from '../library/TankLevel.js'
import WN8vehicle from '../library/WN8vehicle.js'

class VehicleStatAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            battles: "all",
            nameOfVehicles: undefined
        }
        this.Change = this.Change.bind(this)
    }

    componentWillMount() {
        this.getNameOfVehicles()
    }

    Change(event) {
        this.setState({battles: event.target.value})
        event.preventDefault()
        console.log(event.target.value)
    }

    avgValue(battles, variable){
        return (variable / battles).toFixed(4)
    }

    tankyAll() {
        let account_id = Object.keys(this.props.vehicle)
        let vehicles = this.props.vehicle[account_id[0]]
        let id = Object.keys(vehicles)        

        let result = id.map((key) => {            
            if(vehicles[key][this.state.battles].battles !== 0) {
                return <tr className="text-center" key={vehicles[key].tank_id}>
                <TankName tank_id={vehicles[key].tank_id} data={this.state.nameOfVehicles}/>
                <TankLevel tank_id={vehicles[key].tank_id} data={this.state.nameOfVehicles}/>                
                <td className="text-center td-small">{vehicles[key][this.state.battles].battles}</td>
                <td className="text-center td-small">{this.avgValue( vehicles[key][this.state.battles].battles, vehicles[key][this.state.battles].frags)}</td>
                <td className="text-center td-small">{this.avgValue( vehicles[key][this.state.battles].battles,vehicles[key][this.state.battles].damage_dealt)}</td>
                <td className="text-center td-small">{this.avgValue( vehicles[key][this.state.battles].battles,vehicles[key][this.state.battles].spotted)}</td>
                <td className="text-center td-small">{this.avgValue( vehicles[key][this.state.battles].battles,vehicles[key][this.state.battles].dropped_capture_points)}</td>
                <Winrate  wins={vehicles[key][this.state.battles].wins} battles={vehicles[key][this.state.battles].battles}/>
                <WN8vehicle xvm={this.props.xvm} data={vehicles[key][this.state.battles]} tank_id={vehicles[key].tank_id}/>
            </tr>
            }
            
        })

        return result
    }

    getNameOfVehicles(){
        fetch('https://fpcstat.cz/api/Vehicles')
        .then((result)=>{
           return result.json()
        })
        .then((json)=>{
            this.setState({nameOfVehicles: json})            
        })
    }    

    render() {
        
        let vehiclesAll = this.tankyAll()

        return (<div>
            <div className="row small"><h3>Vehicle statistics</h3></div>
            <div className="row">  
            
                <button className="col-2 button small" onClick={this.Change} type="button" value="all" id="load">All</button>
                <button className="col-2 button small" onClick={this.Change} type="button" value="clan" id="load">Clan</button>
                <button className="col-2 button small"  onClick={this.Change} type="button" value="globalmap" id="load">Global map</button>
                <button className="col-2 button small" onClick={this.Change} type="button" value="stronghold_skirmish" id="load">Skirmish</button>
                <button className="col-2 button small" onClick={this.Change} type="button" value="stronghold_defense" id="load">Defense</button>
            
            </div>
            <div className="row">
                <div className="w3-col">
                    <table>
                        <thead>
                            <tr>
                            <th className="text-center">tank name</th>
                            <th className="text-center">level</th>                            
                            <th className="text-center">battles</th>
                            <th className="text-center">kills</th>
                            <th className="text-center">dmg</th>
                            <th className="text-center">spot</th>
                            <th className="text-center">def</th>
                            <th className="text-center">winrate</th>
                            <th className="text-center">wn8</th>
                            </tr>
                        </thead>
                        <tbody>
                            {vehiclesAll}
                        </tbody>
                    </table>
                </div>
            </div>

        </div>)
    }
}

export default VehicleStatAll;