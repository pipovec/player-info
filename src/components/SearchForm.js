import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import * as Actions from "../Actions"

const URL = 'https://api.worldoftanks.eu/wot/account/list/?application_id=883ff6ceefb13177357ffea34d6fb06f&search='

class SearchForm extends Component {

    constructor() {
        super();
        this.state = {                     
          submitText : "Find",
          errorNick: false
        }

    this.findNick = this.findNick.bind(this)
    this.doRequest = this.doRequest.bind(this);
    this.onChangeNickname = this.onChangeNickname.bind(this)
    }
    
    componentDidMount(){
      if(window.id > 0){         
        this.setState({account_id: window.id})
        console.log(window.id)
        Actions.changeNick(window.id)
      }
    }

    findNick(nick) {
        let id;
        fetch(URL + nick, id)
          .then((resp) => {
            return resp.json();
          })
          .then((result) => {                       
            if(result.status === 'ok' && result.meta.count > 0){
              return result.data[0].account_id;
            }
            else{
              this.setState({errorNick: true})
              Actions.changeNick(0)  
            }
            
          })
          .then((account_id) => {
            if(account_id > 0){
              Actions.changeNick(account_id)  
              this.setState({errorNick: false})         
            }
            
          })
    }

    doRequest(event) {
        let nick = this.state.nickname   
        
        event.preventDefault()
        document.title = "Hracske staty | " + nick
        this.findNick(nick)
      }
    
      onChangeNickname(e) {
        this.setState({ nickname: e.target.value, submitText: 'Find'})
      }
    

    render(){
        return(
               
            <Grid container direction="row" justify="flex-start" alignItems="flex-end" spacing={2}>
              <Grid item>
                <TextField onChange={this.onChangeNickname} label="Nick" margin="dense" variant="outlined" error={this.state.errorNick}/>         
              </Grid>                
              <Grid item>
                <Button size='large' onClick={this.doRequest} margin="dense">{this.state.submitText}</Button>        
              </Grid>              
            </Grid>
            
        )
    }
}
export default SearchForm