import React, { Component } from 'react';
import DeltaComponent from './DeltaComponent'

class PlayerStatAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoadForm: false
        }
    }



    readPlayerStatHeaders() {

        let All
        if (typeof this.props.data !== 'undefined') {
            let all = Object.keys(this.props.data)


            All = all.map((k) => {
                return <tr key={k + 989}><td className="text-right td-small">{k}</td><td className="text-center td-small">{this.props.data[k]}</td><DeltaComponent now={this.props.data[k]} past={this.props.snapShot[k]} /></tr>
            })
        }

        return All;
    }


    render() {
        const headers = this.readPlayerStatHeaders();

        return (
            <div>
                <div className="row small"><h3>Player statistics</h3></div>
                <div className="row">

                    <button className="w3-col button small" onClick={this.Change} type="button" value="all" id="load">All</button>
                    <button className="w3-col button small" onClick={this.Change} type="button" value="clan" id="load">Clan</button>
                    <button className="w3-col button small" onClick={this.Change} type="button" value="globalmap" id="load">Global map</button>
                    <button className="w3-col button small" onClick={this.Change} type="button" value="stronghold_skirmish" id="load">Skirmish</button>
                    <button className="w3-col button small" onClick={this.Change} type="button" value="stronghold_defense" id="load">Defense</button>

                </div>
                <table>
                    <thead>
                        <tr>
                            <th className="text-center">Players statistics all</th>
                            <th className="text-center">Now</th>
                            <th className="text-center">Delta</th>
                        </tr>
                    </thead>
                    <tbody className="small">
                        {headers}
                    </tbody>
                </table>
            </div>
        )
    }

}

export default PlayerStatAll;