import React, { Component } from 'react'
import AppStore from "../stores/AppStore"
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

const URL2 = 'https://api.worldoftanks.eu/wot/account/info/?application_id=883ff6ceefb13177357ffea34d6fb06f&account_id='
const URLRecords = 'https://fpcstat.cz/api/calculator/records/'

class PlayerInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account_id: AppStore.getAccount_id(),
            checked: false,
            nickname: '',
            playerStat: {},
            player_history: 0,
            vehicle_history: 0,
            clanLogo: 'https://eu.wargaming.net/clans/media/clans/emblems/cl_018/500019018/emblem_195x195.png'
        }
    
        this.loadPlayerStat = this.loadPlayerStat.bind(this)
        this.HistoryRecords = this.HistoryRecords.bind(this)
    }

    componentDidMount() {
        AppStore.on("storeUpdated", this.updateAccount_id);
    }

    componentWillUnmount() {
        AppStore.removeListener("storeUpdated", this.updateAccount_id);
    }

    updateAccount_id = () => {
        this.setState({account_id: AppStore.getAccount_id(),checked: true})
        if(this.state.account_id > 0){
            this.loadPlayerStat()
            this.HistoryRecords()
        }
        

    }

    loadPlayerStat() {
        fetch(URL2 + this.state.account_id)
        .then((resp) => {
            if (resp.status !== 200) {
                console.log('Chyba, status kod: ' + resp.status);
            }
            else {
                return resp.json();
            }
        })
        .then((result) => {
            if (result.status === "ok") {

                let keys = Object.keys(result.data)
                let data = result.data[keys[0]]
                this.setState({ 
                    playerStat: data,
                    nickname: data.nickname
                })

                console.log(result.data)
            } else {
                this.setState({ playerStat: { "Status": 'not found' } })
            }
        })
    }

    HistoryRecords(){
        fetch(URLRecords + this.state.account_id)
        .then((result)=>{
            return result.json()
        })
        .then((json)=>{
            this.setState({player_history: json.player_history, vehicle_history: json.vehicle_history })
        })
    }
    

    render() {   

        
        return(     
                   
            <Grid container alignItems="center" spacing={5}>
                <Grid item cols={3}>
                    <img src={this.state.clanLogo} alt="logo" height="70px" width="70px" />
                </Grid>
                <Grid item>
                    <Typography style={{fontSize: '2rem'}}>
                    {this.state.playerStat.nickname}
                    </Typography>
                    
                </Grid>                       
            </Grid>
            
            
        );
    };


}

export default PlayerInfo;
