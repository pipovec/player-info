import dispatcher from "./Dispatcher";

export const NICK_APP_ACTIONS = {
    CHANGE_NICK: 'nickAppActions.ChangeNick'
};

export function changeNick(nickName) {
    dispatcher.dispatch({
        type: NICK_APP_ACTIONS.CHANGE_NICK,
        value: nickName
    })
}