import React, { Component } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline'
import SearchFrom from './components/SearchForm'
import PlayerInfo from './components/PlayerInfo'


/* const URL3 = 'https://api.worldoftanks.eu/wot/tanks/stats/?application_id=883ff6ceefb13177357ffea34d6fb06f'
const vehicleFileds = 'tank_id,all.battles,all.damage_dealt,all.frags,all.wins,all.spotted,all.dropped_capture_points'
const xvm = 'https://static.modxvm.com/wn8-data-exp/json/wn8exp.json' */

class App extends Component {
  

  /* loadVehicleStat(id){
    
    fetch(URL3, {
      method: 'POST',      
      body: "&account_id="+id+"&fields"+vehicleFileds 
    })
    .then((res) => res.json())      
    .then((result) => {
      
      this.setState({statVehicle: true, vehicleStat: result.data })
    });

  } */

  /** Nahranie a ulozenie XVM hodnot  */
  /* loadXvmValues() {    
    fetch(xvm)
    .then((result)=> result.json())
  } */

  render() {
   
    return (
      <CssBaseline>
        <SearchFrom></SearchFrom>
        <hr />
        <PlayerInfo></PlayerInfo>
        <hr />
      </CssBaseline>
    );
  }
}

export default App;
