import React, { Component } from 'react';

class TankLevel extends Component {

    render(){
    var level
    if(typeof this.props.data !== 'undefined'){
        for(let i=0; i < this.props.data["data"].length; i++){
            if (this.props.tank_id === this.props.data["data"][i].tank_id){
                level = this.props.data["data"][i].level
            }
        }       
    }
       

        return(
            <td className="text-center td-small">{level}</td>
        )}

}

export default TankLevel