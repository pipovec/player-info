import React, { Component } from 'react';

class TankName extends Component {

    render(){
    var name
    if(typeof this.props.data !== 'undefined'){
        for(let i=0; i < this.props.data["data"].length; i++){
            if (this.props.tank_id === this.props.data["data"][i].tank_id){
                name = this.props.data["data"][i].name
            }
        }       
    }
       

        return(
            <td className="text-center td-small">{name}</td>
        )}

}

export default TankName