/* Komponenta na vypocet WN8 pre jeden tank */

/**
 * Vypocita hodnotu WN8 pre 1 tank
 * @param {array} xvm - XVM hodnoty vsetky
 * @param {object} data - hodnoty jedneho tanku
 * @returns {float} WN8 - WN8 obalena tagmi <td></td>
 */
import React, { Component } from 'react';


/**
 * 
 * @param {*} expValue 
 * @param {*} Value 
 */
function rDMGc(expValue, Value) {
    let rDMG = Value / expValue
    let rDMGc = Math.max(0, (rDMG - 0.22) / (1 - 0.22))

    if(isNaN(rDMGc))  {
        rDMGc = 0.0
    }

    return rDMGc
}

/**
 * @param {float} expWin
 * @param {float} Win  
 */
function rWinc(expWin, Win) {
    var rWIN = Win / expWin;
    rWIN = parseFloat(rWIN);
    var rWINc = Math.max(0, (rWIN - 0.71) / (1 - 0.71));

    if(isNaN(rWINc))  {
        rWINc = 0.0
    }

    return rWINc
}
/**
 * 
 * @param {float} expSpot 
 * @param {float} Spot 
 * @param {float} rDMGc 
 */
function rSpotc(expSpot, Spot, rDMGc) {
    let rSpot = Spot / expSpot
    let rSpotc = Math.max(0, Math.min(rDMGc + 0.1, (rSpot - 0.38) / (1 - 0.38)));

    if(isNaN(rSpotc))  {
        rSpotc = 0.0
    }

    return rSpotc.toFixed(4)
}

/**
 * 
 * @param {float} expDef 
 * @param {float} Def 
 * @param {float} rDMGc 
 */
function rDEFc(expDef, Def, rDMGc) {
    let rDef = Def / expDef
    let rDEFc = Math.max(0, Math.min(rDMGc + 0.1, (rDef - 0.10) / (1 - 0.10)));

    if(isNaN(rDEFc))  {
        rDEFc = 0.0
    }

    return rDEFc.toFixed(4)
}

/**
 * 
 * @param {float} expFrag 
 * @param {float} Frag 
 * @param {float} rDMGc 
 */
function rFragc(expFrag, Frag, rDMGc) {
    let rFrag = Frag / expFrag
    let rFragc = Math.max(0, Math.min(rDMGc + 0.2, (rFrag - 0.12) / (1 - 0.12)));

    if(isNaN(rFragc))  {
        rFragc = 0.0
    }

    return rFragc.toFixed(4)
}

function WN8(expValues, data, tank_id) {
    let xvm = expValues.data
    var expDef, expFrag, expSpot, expDmg, expWin

    for (let i = 0; i < xvm.length; i++) {
        if (xvm[i].IDNum === tank_id) {
            expDef  = xvm[i]['expDef']
            expFrag = xvm[i]['expFrag']
            expSpot = xvm[i]['expSpot']
            expDmg  = xvm[i]['expDamage']
            expWin  = xvm[i]['expWinRate']
            break
        }
    }
    
    // Vypocitat priemerne hodnoty na jednu bitku
    let avgdmg  = data.damage_dealt / data.battles
    let avgwin  = (data.wins / data.battles) * 100
    let avgspot = data.spotted / data.battles
    let avgdef  = data.dropped_capture_points / data.battles
    let avgfrag = data.frags / data.battles


    
    let rdmgc   = rDMGc(expDmg, avgdmg)
    let rwinc   = rWinc(expWin, avgwin)
    let rspotc  = rSpotc(expSpot,avgspot,rdmgc)
    let rdefc   = rDEFc(expDmg, avgdef, rdmgc)
    let rfragc  = rFragc(expFrag, avgfrag)

    var WN8 = 980*rdmgc + 210*rdmgc*rfragc + 155*rfragc*rspotc + 75*rdefc*rfragc + 145*Math.min(1.8,rwinc);
    return WN8
}

/**
 * 
 * @param {float} wn8 
 */
function returnWn8Color(wn8){
    let Wcolor
    if(wn8 <= 452) {
        Wcolor = 'bg-color-xvm-wn-452';
      } else if (wn8 <= 983) {
        Wcolor = 'bg-color-xvm-wn-983';
      } else if (wn8 <= 1577) {
        Wcolor = 'bg-color-xvm-wn-1577';
      } else if (wn8 <= 2377) {
        Wcolor = 'bg-color-xvm-wn-2377';
      } else if (wn8 <= 3199) {
        Wcolor = 'bg-color-xvm-wn-3199';
      } else if (wn8 > 3200) {
        Wcolor = '#bg-color-xvm-wn-3200';
  } 

  return Wcolor
}


class WN8vehicle extends Component {

    render() {
        var wn8 = 0.00
        if (this.props.data !== 'undefined') {
          wn8 =  WN8(this.props.xvm, this.props.data, this.props.tank_id)
        }

        let color = returnWn8Color(wn8)

        return (
            <td className={color}>{wn8.toFixed(2)}</td>
        )
    }
}

export default WN8vehicle