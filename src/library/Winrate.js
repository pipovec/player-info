import React, { Component } from 'react';


class Winrate extends Component {

winrateColor (winrate) {
    let color = "bg-color-xvm-win-0";

    if ( winrate >= 43.45 && winrate <= 46.38)
        color = "bg-color-xvm-win-46"
    else if(winrate >= 46.39 && winrate <= 49.15 )
        color = "bg-color-xvm-win-49"
    else if(winrate >= 49.16 && winrate <= 52.49)
        color = "bg-color-xvm-win-53"
    else if(winrate >= 52.50 && winrate <= 57.77 )
        color = "bg-color-xvm-win-58"
    else if(winrate >= 57.78 && winrate <= 63.55 )
        color = "bg-color-xvm-win-64"
    else if(winrate >= 63.56 )
        color = "bg-color-xvm-win-100"
   

    
    return color;
}

render() {
    let winrate = (this.props.wins / this.props.battles) * 100
    let s = this.winrateColor(winrate)
    
    return (
        <td className={s} >{winrate.toFixed(2)}</td>
    )
}

}

export default Winrate