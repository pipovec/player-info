import dispatcher from "../Dispatcher";
import {EventEmitter} from "events";
import * as Actions from "../Actions";

class AppStore extends EventEmitter {

    constructor() {
        super();
        this.account_id = ''
    }

    handleActions(action) {
        switch (action.type) {
            case Actions.NICK_APP_ACTIONS.CHANGE_NICK: {
                this.account_id = action.value;
                this.emit("storeUpdated");
                break;
            }
            default: {
            }
        }
    }

    getAccount_id() {
        return this.account_id;
    }
}

const appStore = new AppStore()
dispatcher.register(appStore.handleActions.bind(appStore))
export default appStore